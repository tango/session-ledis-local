session-ledis-local [![Build Status](https://drone.gitea.com/api/badges/tango/session-ledis-local/status.svg)](https://drone.gitea.com/tango/session-ledis-local) [![](http://gocover.io/_badge/gitea.com/tango/session-ledis-local)](http://gocover.io/gitea.com/tango/session-ledis-local)
======

Session-ledis-local is a store of [session](https://gitea.com/tango/session) middleware for [Tango](https://gitea.com/lunny/tango) stored session data via [ledisdb](http://github.com/ledisdb/ledisdb). 

## Installation

    go get gitea.com/tango/session-ledis-local

## Simple Example

```Go
package main

import (
    "gitea.com/lunny/tango"
    "gitea.com/tango/session"
    "gitea.com/tango/session-ledis-local"
)

type SessionAction struct {
    session.Session
}

func (a *SessionAction) Get() string {
    a.Session.Set("test", "1")
    return a.Session.Get("test").(string)
}

func main() {
    o := tango.Classic()
    store, _ := ledislocal.New(ledislocal.Options{
        Path:    "./ledis_store",
        DBIndex: 0,
        MaxAge:  30 * time.Minute,
    })
    o.Use(session.New(session.Options{
        Store: store,
        }))
    o.Get("/", new(SessionAction))
}
```

## Getting Help

- [API Reference](https://godoc.org/gitea.com/tango/session-ledis-local)
