// Copyright 2020 The Tango Authors. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package ledislocal

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"reflect"
	"time"
	"unsafe"

	"gitea.com/lunny/log"
	"gitea.com/lunny/tango"
	"gitea.com/tango/session"
	"github.com/ledisdb/ledisdb/config"
	"github.com/ledisdb/ledisdb/ledis"
)

var _ session.Store = &LedisLocalStore{}

type Options struct {
	Path    string
	DBIndex int
	MaxAge  time.Duration
}

// LedisLocalStore represents a ledis local session store implementation.
type LedisLocalStore struct {
	Options
	tango.Logger
	db *ledis.DB
}

func preOptions(opts []Options) Options {
	var opt Options
	if len(opts) > 0 {
		opt = opts[0]
	}
	if opt.Path == "" {
		opt.Path = "./ledis_store"
	}
	if opt.MaxAge == 0 {
		opt.MaxAge = session.DefaultMaxAge
	}
	return opt
}

// New creates and returns a redis session store.
func New(opts ...Options) (*LedisLocalStore, error) {
	opt := preOptions(opts)
	cfg := config.NewConfigDefault()
	cfg.DataDir = opt.Path

	ndb, err := ledis.Open(cfg)
	if err != nil {
		return nil, err
	}
	db, err := ndb.Select(opt.DBIndex)
	if err != nil {
		return nil, err
	}

	return &LedisLocalStore{
		Options: opt,
		db:      db,
		Logger:  log.Std,
	}, nil
}

func (c *LedisLocalStore) serialize(value interface{}) ([]byte, error) {
	err := c.registerGobConcreteType(value)
	if err != nil {
		return nil, err
	}

	if reflect.TypeOf(value).Kind() == reflect.Struct {
		return nil, fmt.Errorf("serialize func only take pointer of a struct")
	}

	var b bytes.Buffer
	encoder := gob.NewEncoder(&b)

	err = encoder.Encode(&value)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (c *LedisLocalStore) deserialize(byt []byte) (ptr interface{}, err error) {
	b := bytes.NewBuffer(byt)
	decoder := gob.NewDecoder(b)

	var p interface{}
	err = decoder.Decode(&p)
	if err != nil {
		return
	}

	v := reflect.ValueOf(p)
	if v.Kind() == reflect.Struct {
		var pp interface{} = &p
		datas := reflect.ValueOf(pp).Elem().InterfaceData()

		p := unsafe.Pointer(datas[1])
		sp := reflect.NewAt(v.Type(), p).Interface()
		ptr = sp
	} else {
		ptr = p
	}
	return
}

func (c *LedisLocalStore) registerGobConcreteType(value interface{}) error {
	t := reflect.TypeOf(value)

	switch t.Kind() {
	case reflect.Ptr:
		v := reflect.ValueOf(value)
		i := v.Elem().Interface()
		gob.Register(i)
	case reflect.Struct, reflect.Map, reflect.Slice:
		gob.Register(value)
	case reflect.String, reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64, reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Bool, reflect.Float32, reflect.Float64, reflect.Complex64, reflect.Complex128:
		// do nothing since already registered known type
	default:
		return fmt.Errorf("unhandled type: %v", t)
	}
	return nil
}

// Set sets value to given key in session.
func (s *LedisLocalStore) Set(id session.Id, key string, val interface{}) error {
	bs, err := s.serialize(val)
	if err != nil {
		return err
	}
	_, err = s.db.HSet([]byte(id), []byte(key), bs)
	if err == nil {
		// when write data, reset maxage
		_, err = s.db.HExpire([]byte(id), int64(s.MaxAge.Seconds()))
	}
	return err
}

// Get gets value by given key in session.
func (s *LedisLocalStore) Get(id session.Id, key string) interface{} {
	val, err := s.db.HGet([]byte(id), []byte(key))
	// if not exist
	if err == ledis.ErrLogMissed {
		return nil
	}

	if err != nil {
		s.Logger.Errorf("nodb HGET failed: %s", err)
		return nil
	}

	// when read data, reset maxage
	s.db.HExpire([]byte(id), int64(s.MaxAge.Seconds()))

	if len(val) == 0 {
		return nil
	}

	value, err := s.deserialize(val)
	if err != nil {
		s.Logger.Errorf("nodb HGET failed: %v - %v", err, val)
		return nil
	}
	return value
}

// Keys gets all keys of the session.
func (s *LedisLocalStore) Keys(id session.Id) ([]string, error) {
	vals, err := s.db.HKeys([]byte(id))
	// if not exist
	if err == ledis.ErrLogMissed {
		return []string{}, nil
	}

	if err != nil {
		return nil, err
	}

	// when read data, reset maxage
	s.db.HExpire([]byte(id), int64(s.MaxAge.Seconds()))
	if len(vals) == 0 {
		return []string{}, nil
	}

	var res = make([]string, 0, len(vals))
	for _, v := range vals {
		if string(v) != string(id) {
			res = append(res, string(v))
		}
	}
	return res, nil
}

// Del delete a key from session.
func (s *LedisLocalStore) Del(id session.Id, key string) bool {
	_, err := s.db.HDel([]byte(id), []byte(key))
	return err == nil
}

func (s *LedisLocalStore) Clear(id session.Id) bool {
	_, err := s.db.Del([]byte(id))
	return err == nil
}

func (s *LedisLocalStore) Add(id session.Id) bool {
	_, err := s.db.HSet([]byte(id), []byte(id), []byte(""))
	if err == nil {
		// when write data, reset maxage
		_, err = s.db.HExpire([]byte(id), int64(s.MaxAge.Seconds()))
	}

	return err == nil
}

func (s *LedisLocalStore) Exist(id session.Id) bool {
	b, _ := s.db.HLen([]byte(id))
	return b > 0
}

func (s *LedisLocalStore) SetMaxAge(maxAge time.Duration) {
	s.MaxAge = maxAge
}

func (s *LedisLocalStore) SetIdMaxAge(id session.Id, maxAge time.Duration) {
	if s.Exist(id) {
		s.db.HExpire([]byte(id), int64(s.MaxAge.Seconds()))
	}
}

func (s *LedisLocalStore) Run() error {
	return nil
}
