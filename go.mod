module gitea.com/tango/session-ledis-local

go 1.14

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e
	gitea.com/lunny/tango v0.6.2
	gitea.com/tango/session v0.0.0-20201110080243-87f6e468e457
	github.com/ledisdb/ledisdb v0.0.0-20200510135210-d35789ec47e6
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
)
